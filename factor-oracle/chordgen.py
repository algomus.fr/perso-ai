# -*- coding: utf-8 -*-
# Python 3.5

'''
Chord generation with a factor oracle

This file is part of "Personalizing AI for co-creative music composition" 
  <http://algomus.fr/code>
  Copyright (C) 2015-2022 by 
      Ken Deguernel <ken.deguernel@discordia.fr>
      Algomus, CRIStAL, Univ. Lille.
      Based on work done at Inria Nancy - Grand Est and Ircam.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with the software. If not, see http://www.gnu.org/licenses/
'''


import os
import sys
from utils import *
from oracle import *

def main():

    # Reading txt files to extract the list of chords
    chord_list = []

    # Define the file which will be the starting point of the oracle (optional)
    if len(sys.argv) == 3:
        temp_chord_list = textParser(sys.argv[2])
        chord_list = chord_list + temp_chord_list

    # Construct an factor oracle from a list of chord progressions
    for file in os.listdir(sys.argv[1]):
        file_name = sys.argv[1]+'/'+file
        temp_chord_list = textParser(file_name)
        chord_list = chord_list + temp_chord_list

    # Building the FactorOracle
    pythie = FactorOracle(chord_list, chord_list)

    # Improvisation parameters
    improv_length = 32
    continuity_factor_min = 2
    taboo_list_length = 8
    context_length_min = 2

    # Generation using a non-probabilistic factor oracle
    improv = pythie.classicPath(improv_length, continuity_factor_min, taboo_list_length, context_length_min)

    # Display
    chords = ""
    i=0
    for chord in improv:
        i = i+1
        chords = chords + chord + '\t'
        if i == 4:
            i = 0
            chords = chords + '\n'

    print(chords)

if __name__ == '__main__': main()