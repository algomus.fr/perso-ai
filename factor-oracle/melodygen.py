#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Python 3.5

'''
Melody generation on a chord progression with a factor oracle

This file is part of "Personalizing AI for co-creative music composition" 
  <http://algomus.fr/code>
  Copyright (C) 2015-2022 by 
      Ken Deguernel <ken.deguernel@discordia.fr>
      Algomus, CRIStAL, Univ. Lille.
      Based on work done at Inria Nancy - Grand Est and Ircam.

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with the software. If not, see http://www.gnu.org/licenses/
'''

from utils import *
from proba import *
from oracle import *

def main():

	# Probabilistic model construction with save file (dummy data on classical music corpus)
	bigram = SubModel("bigram", "data/melody-trained-classical/bigram_save.xml")
	melody_chord = SubModel("melody_chord","data/melody-trained-classical/melody_chord_save.xml")
	# Parameters for weight of each model in the interpolation
	proba_model = Model([bigram, melody_chord],[0.3, 0.7])

	# Oracle construction
	note_list = []
	for file in os.listdir(sys.argv[1]):
		file_name = sys.argv[1]+"/"+file
		(score_info, _, note_list_temp) = xml_parser(file_name)
	# CHANGEMENT DUREE
		for note in note_list_temp:
			note.duration = score_info.divisions/2
		note_list = note_list + note_list_temp


	note_labels = []
	for note in note_list:	# We choose integer representing pitch relative to octave as labels.
		note_labels.append(note2Label(note))
	pythie = FactorOracle(note_labels, note_list)

	# Improvisation parameters
	improv_length = 64
	continuity_factor_min = 4
	taboo_list_length = 2
	context_length_min = 2

	# Scenario
	scenario_name = sys.argv[2]
	(_,chord_list,_) = xml_parser(scenario_name)
	chord_list = chord_list + chord_list
	scenario = [[chord2Label(chord), chord.timestamp] for chord in chord_list]

	# Improvisation
	improv = pythie.informedPath(improv_length, continuity_factor_min, taboo_list_length, context_length_min, proba_model, scenario)

	# OpenMusic output
	#improv2OM(score_info, improv)

	#musicXML output
	back2xml(score_info, improv)


######## ######## ########

if __name__ == '__main__': main()
