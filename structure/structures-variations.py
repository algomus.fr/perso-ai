'''
Generation of structure and variations using Markov rules and variation profiles

This file is part of "Personalizing AI for co-creative music composition" 
  <http://algomus.fr/code>
  Copyright (C) 2020-2022 by 
      Mathieu Giraud <mathieu@algomus.fr>
      Richard Groult <richard@algomus.fr>
      CRIStAL, Univ. Lille and LITIS, Univ. Rouen Normandie

  This is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with the software. If not, see http://www.gnu.org/licenses/
'''

import random
import datetime
import sys


def possibly_weighted_choices(l, k=1, unique=False):
    '''
    [x1, x2, ...] => returns a random choice
    [(x1, w1), (x2,w2), ...] => returns a weighted choice
    '''

    if k == 0:
        return []

    if type(l[0]) == type((0,0)):
        if len(l[0]) == 3:
            choices, weights, doc = zip(*l)
        else:
            choices, weights = zip(*l)
        c = random.choices(choices, weights, k=k)
    else:
        c = random.choices(l, k=k)

    if k > 1 and unique:
        if max([ c.count(cc) for cc in c]) >= 2:
            return possibly_weighted_choices(l, k, unique)

    return c

pwc = possibly_weighted_choices


def str_keylist(key, l):
    return "%8s " % key + ''.join(map(lambda x: "%8s" % x, l)) + '\n'

def occs(s):
    return sorted([s.count(c) for c in s])


class StructureVariationGen:

    def __init__(self):
        self.reset()

    def reset(self):
        self.base = ""
        self.mods = []
        self.states = []
        self.chords = []
        self.curves = {}
        self.n = 0

    def markov(self):
        state = 0
        self.reset()
        STOP = len(self.STATES)-1
        while state != STOP:
            # store previous state
            self.base += self.STATES[state]

            state = random.choices(list(range(len(self.STATES))), self.MARKOV[state])[0]

            # Do not output C before B has been output
            if state > 0 and state != STOP:
                while state > 0 and state != STOP and not self.STATES[state-1] in self.base:
                    state = random.choices(list(range(len(self.STATES))), self.MARKOV[state])[0]

    def details(self):
        self.n = 0
        self.states = []
        self.chords = []
        for state in self.base:
            (name, chords) = self.STATES_DETAILS[state]
            self.n += 1
            self.states += [name]
            self.chords += chords.split()

    def ok_lim(self, bottop, bottop_chords):
        '''Does the generation fit into some limits ?'''
        bot, top = bottop
        botc, topc = bottop_chords

        if self.n < bot or self.n > top:
            return False

        if len(self.chords) < botc or len(self.chords) > topc:
            return False

        if len(set(self.base)) < 3 or occs(self.base)[1] < 2:
            return False

        if not 'D' in self.base:
            return False

        return True

    def markov_lim(self, bottop, bottop_chords, verbose=False):
        while not self.ok_lim(bottop, bottop_chords):
            if verbose:
               sys.stdout.write('.')
               sys.stdout.flush()
            self.markov()
            self.details()
        if verbose:
            print()

    def gen_curve(self, curves):
        curve = pwc(curves)[0]
        c = len(curve)
        return ''.join(curve[int(i*c/self.n)] for i in range(self.n))

    def gen(self, name='', verbose=False):
        self.reset()
        self.markov_lim(self.N_LIM, self.CHORDS_LIM, verbose)
        self.gen_variations(name)

    def gen_variations(self, name=''):
        self.name = name
        self.n = len(self.base)
        self.mods = [[] for i in range(self.n)]
        for key, (curves, doc) in self.CURVES.items():
            self.curves[key] = self.gen_curve(curves)

        for key, (curves, mods, allow_first, doc) in self.CURVES_KEYWS.items():
            cc = self.gen_curve(curves)
            for i, c in enumerate(cc):
                if i == 0:
                    # No mods on first section
                    continue
                if allow_first or (self.base[i] in self.base[:i]):
                    mm = pwc(mods, k=int(c), unique=True)
                    self.mods[i] += mm
    
    def doc(self):
        s = ''
        for key, (curves, doc) in self.CURVES.items()   :
            s += '    %-10s %s\n' % (key, doc)
        for key, (curves, mods, allow_first, doc) in self.CURVES_KEYWS.items():
            s += '\n=== %s %s\n' % (key, doc)
            for k, p, doc in mods:
                s += '    %-10s %s\n' % (k, doc)
        return s

    def pretty_states(self):
        '''join without space on same consecutive state'''
        s = ''
        previous = None
        for st in self.states:
            if previous and previous != st:
                s += ' '
            s += st
            previous = st
        return s

    def __str__(self):
        s = '----- %s: %s [%d sections, %d chords]\n' % (self.name, self.pretty_states(), self.n, len(self.chords))
        for i in range(self.n):
            for key, curve in self.curves.items():
                s += key + curve[i] + ' '
            (name, chords)= self.STATES_DETAILS[self.base[i]]
            s += "%-2s" % name
            # s += " %-20s" % chords

            if self.mods[i]:
                s += '(%s)' % ','.join(self.mods[i])
            s += '\n'

        # s += ','.join(self.chords)
        return s

########

class Apr21(StructureVariationGen):
    '''
    Example model, partially used to generate the structure
    and the variations for "The last moment before you fly

    A model has to subclass `StructureGen`
    '''

    STATES = "aABCDEZ"

    ## Some chord sequences
    # Used here only to check the CHORDS_LIM,
    # and to pretty-print the structure
    
    STATES_DETAILS = {
        'a': ('A',  'Dm C F Bb Bb'),
        'A': ('A',  'Dm C F Bb Bb'),
        'B': ('B',  'Dm C Am Bb Dm/F G A Em'),
        'C': ('C',  'C D Em Em C Bm/D Em G ' * 2),
        'D': ('D1', 'Eb Cm Db Db Eb Cm Db Db Fm Fm'),
        'E': ('D2', 'Db Ab Eb Db Db Fm Eb Cm Db Bbm Fm Fm')
    }

    # Limits
    N_LIM = (7, 22)         # Number of sections
    CHORDS_LIM = (80, 140)  # Number of chords

    ## An example of semi-explicit rules to chain sections
    # - a at the beginning (preferably several times), then A
    # - B can be used after C, and possibly A 
    # - C can be used after D, E, A, B
    # - D before E
    # - B goes to C rather than A

    STOP = 1
    NOSTOP = 0

    MARKOV = [ [  1, 3, 0, 0, 0, 0, NOSTOP], # a (initial state)
               [  0, 3, 1, 5, 0, 0, STOP], # A     
               [  0, 1, 0, 5, 0, 0, STOP], # B
               [  0, 1, 1, 0, 3, 9, STOP], # C
               [  0, 1, 1, 0, 0, 0, STOP], # D
               [  0, 1, 1, 0, 0, 0, STOP], # E
               [  0, 0, 0, 0, 0, 0, 1]]    # Z (final state)


    ### Variation curves // 'integer' curves

    # Stereo, wide/close
    CURVES_S = [ 
        ("012", 0.3),
        ("0001111222211122", 0.3),
        ("00001111110022", 0.2),
        ("0011122110011122", 0.2),
        ("000000111222", 0.2),
    ]

    # Timber/texture, dark/light
    CURVES_T = [
        ("222211111100001112222", 0.3),
        ("00001111110022", 0.2),
        ("001112220001122", 0.2),
        ("001111222", 0.2),
    ]

    # Now define CURVES
    CURVES = { 
        'Ste-': (CURVES_S, "Ste-0/1/2\tstereo: wide/close"),
        'Timb-': (CURVES_T, "Timb-0/1/2\ttimber: dark/light"),
    }

    ### Variation curves // 'keyword' curves, using personalized terms

    # Harmony
    KEYW_H = [
        ("H-subst", 4, "chord substitution"),
        ("H-modul", 1, "modulation"),
    ]

    # Rhythm
    KEYW_R = [
        ("R-dense", 4, "densification"),
        ("R-light", 3, "lightening"),
        ("R-frag",  2, "conversion, fragmentation"),
    ]

    # Melody
    KEYW_M = [
        ("M-trans", 2, "diatonic transposition"),
        ("M-irreg", 2, "irregular copy"),
        ("M-ornem", 3, "ornementation, appogiaturas"),
    ]

    # Timbre, texture
    KEYW_T = [
        ("T-far",   4, "distancing"),
        ("T-near",  6, "rapprochment"),
        ("T-subst", 4, "substitution"),
    ]


    # Now define CURVES_KEYWS
    CURVES_KEYW_H = [
        ("00010011001", 0.4),
        ("00000100010", 0.4),
        ("0001", 0.3),
    ]
    CURVES_KEYW_T = [
        ("010010101", 0.4),
        ("100101", 0.4),
        ("001001101", 0.3),
    ]
    CURVES_KEYW_M = [
        ("0000111222", 0.4),
        ("00001101", 0.4),
        ("0010112", 0.3),
    ]
    CURVES_KEYW_R = [
        ("001011", 0.4),
        ("0110011", 0.4),
        ("00110011", 0.3),
    ]
    CURVES_KEYWS = {
        'H': (CURVES_KEYW_H, KEYW_H, False, 'Harmony'),
        'R': (CURVES_KEYW_R, KEYW_R, False,  'Rythm'),
        'M': (CURVES_KEYW_M, KEYW_M, False, 'Melody'),
        'T': (CURVES_KEYW_T, KEYW_T, True,  'Timbre/Texture'),
    }




model = Apr21()

print(model.doc())

def structures_and_variations(start = 0, n=5, verbose=False):
    for i in range(n):
        g = model.gen("Structure %02d" % (start + i), verbose=verbose)
        print(model)

def variations(start, base, n=5):
    model.base = base
    model.details()
    for i in range(n):
        model.gen_variations("Structure %02d-%02s" % (start, i))
        print(model)



if __name__ == '__main__':
    # Generate some structures, starting from 0 (with MARKOV and *_LIM),
    # generating also one variation on each structure
    structures_and_variations(0)

    # Generate some variations on a given structure
    variations(24, 'AABCACDBCD')

    print(datetime.datetime.now().isoformat(timespec='seconds'))
